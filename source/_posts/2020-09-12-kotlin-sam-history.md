---
title: 关于Kotlin的SAM转换的破事水
tags: Kotlin
date: 2020-09-12 15:12:43
---

随着 Kotlin 1.4 正式发布，关于 SAM 转换的一些问题就可以盖棺定论了。因为这里要讲的都是些旧的东西，所以这是一篇灌水文。<!--more-->

## Kotlin对SAM转换的支持情况

在 1.4 发布之前，经常有新人在群里提出关于 SAM 转换的问题。

为了说明这个问题，要分成几个情况来讨论。

我们需要区分这个接口是Java接口还是Kotlin接口：

``` java
// 这是Java
interface JavaSome {
   void some(); 
}
```
``` kotlin
// 这是Kotlin
interface KotlinSome {
   fun some()
}
```

以及区分在Java还是Kotlin里使用该接口：

``` java
// 这是Java, ISome是一个接口
void useSome(ISome some) {}
```
``` kotlin
// 这是Kotlin, ISome是一个接口
fun useSome(some: ISome) {}
```

两两相乘，我们就需要对4种情况进行讨论。当然，`useSome` 函数都是在 Kotlin 里调用。

### 1、Java接口，Java使用

``` java
// Java
void useSome(JavaSome some) {}
```
``` kotlin
// Kotlin
useSome {} // OK
```

这种情况下的 SAM 转换，是自古以来 Kotlin 就支持的。

### 2、Java接口，Kotlin使用

``` kotlin
// Kotlin
fun useSome(some: JavaSome) {}

useSome {} // 能否编译成功跟Kotlin版本和编译器参数有关
```

Kotlin 1.2 以及更旧版本不支持这种情况下的SAM转换。

Kotlin 1.3 版本，Kotlin 官方团队发现他们写的那堆类型推断算法是一座“屎山”，于是重新写了套新的类型推断算法，作为默认关闭的实验性特性加入了 1.3 版本。新的类型推断算法支持这种情况下的SAM转换，不过需要[手动传入编译器参数来开启这个功能](https://aisia.moe/2018/01/07/kotlin-jiqiao/#%E5%BC%80%E5%90%AF%E6%AE%8B%E5%BA%9F%E7%9A%84SAM%E8%BD%AC%E6%8D%A2%E5%8A%9F%E8%83%BD)。

Kotlin 1.4 版本，由于新的类型推断算法已经默认开启，所以这种情况下可以进行SAM转换。

### 3、Kotlin接口，Kotlin使用

``` kotlin
// Kotlin
fun useSome(some: KotlinSome) {}

useSome {} // 编译错误！
```

这就是广为人知、为人诟病的垃圾 Kotlin 不支持 SAM 转换的情况。

在 Kotlin 1.4 版本，你需要在接口前加上关键字 fun，让它成为一个 fun interface 才能享受到 SAM 转换。

``` kotlin
// Kotlin
fun interface KotlinSome {
   fun some()
}

fun useSome(some: KotlinSome) {}

useSome {} // OK
```

当然 1.3 版本就别想了，老老实实升级吧。

### 4、Kotlin接口，Java使用

``` java
// Java
void useSome(KotlinSome some) {}
```
``` kotlin
// Kotlin
useSome {} // 需要是 fun interface
```

非常少见。

和上面的第三种情况一样，这需要 Kotlin 1.4 版本的 fun interface 才能进行 SAM 转换。

### 5、带有suspend函数的Kotlin接口

~~四天王有五个人不是常识么~~

``` kotlin
fun interface Some {
   suspend fun some()
}

fun useSome(some: KotlinSome) {}

useSome {} // 嘻嘻
```

在 Kotlin 1.4 的测试版（里程碑版、RC版），可以编译成功，但是运行起来会炸。原因在于 Kotlin 官方团队并没有写好针对这种情况的代码生成（codegen）。于是在 Kotlin 1.4 正式版，他们就 ban 掉了这样的代码，不允许 `fun interface` 拥有抽象 `suspend` 函数。

### 6、一些旧版本的bug

最经典的是那个安卓的LiveData的某个函数：

``` kotlin
val liveData = MutableLiveData<Int>()
liveData.observe({ lifecycleOwner.lifecycle }, Observer { invokeMyMethod(it) })
// 第二个参数无法进行SAM转换
```

详见[KT-14984](https://youtrack.jetbrains.com/issue/KT-14984)。

新的类型推断算法修正了这个bug。

## SAM Constructor

在 1.3 以及更早的版本，针对上面所说的第二种情况，可以这样使用：

``` kotlin
// Kotlin
fun useSome(some: JavaSome) {}

useSome(JavaSome {})
```

想必各位过来人都知道这样的写法。

这里 `JavaSome {}`，lambda 表达式前面的那个 `JavaSome` 就是所谓的 SAM 构造器（SAM constructor），或者说是 SAM 适配器（SAM adapter）。

在现在 1.4 版本里，SAM constructor 已经没什么用了，主要用途是“凭空捏出”一个 SAM 接口的实例：

``` kotlin
val ktSome = KotlinSome {} // 需要是 fun interface
val javaSome = JavaSome {}

// 错误用法
// val ktSome: KotlinSome = {}
// val javaSome: JavaSome = {}
```

SAM constructor 可以理解为编译器为 SAM 接口生成了一个如下所示的辅助函数，但是实际上这个函数并不存在。

``` java
// 这是Java
interface JavaSome {
   void some(); 
}
```

``` kotlin
// 实际上并不存在的辅助函数
inline fun JavaSome(block: () -> Unit): JavaSome {
   return 编译器的魔法
}
```

然后就有一些鲜为人知的用法，比如说这样：

``` kotlin
// Kotlin
val lambda: () -> Unit = { println("test") }
val kepa: JavaSome = JavaSome(lambda) // 嘻嘻
kepa.some() // 输出 test
```

上面这段代码确实是可以跑的。

甚至是这样：

``` kotlin
val lambda: () -> Unit = { println("test") }
val some: KFunction1<() -> Unit, JavaSome> = ::JavaSome // 嘻嘻
val kepa: JavaSome = some.invoke(lambda)
kepa.some()
```

这段代码 IDEA 不会提示错误，但是会编译失败。

表面上看确实有这个辅助函数，所以这样的代码可以通过 Kotlin 编译器前端的检查。但是实际上编译器的后端并没有办法针对这样的情况进行代码生成，彻底懵逼了，boom！

## 你学到了什么

* 一些无用的历史知识
* 关于 SAM constructor 的冷知识

本文完。
