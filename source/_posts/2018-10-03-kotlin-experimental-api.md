---
title: Kotlin Experimental API
tags: Kotlin
date: 2018-10-03 11:55:15
---

本文随便讲讲Kotlin Experimental API。<!--more-->

为了防止篇幅太大，本文会省略一些无关紧要的东西。

## 简单介绍

首先我们知道在Kotlin的1.2版本里协程是实验性的，协程的那个包名里面有个十分显眼的experimental字样，用于告诉使用者这破玩意处于实验阶段。

然后，JB那帮人嫌这个标识方法很不方便，等到协程正式发布后会给迁移带来一定的麻烦。同时用包名声明实验性的这个方法太过粗犷，他们想要一种细粒度的（比如说可以对单个类甚至是单个函数）标记实验性质的机制。

所以就有了 Kotlin Experimental API。

## 简单使用

Kotlin 1.3 版本的标准库里新增了几个注解，如下

``` kotlin
// Kotlin 标准库里的东西
@Target(ANNOTATION_CLASS)
annotation class Experimental(val level: Level = Level.ERROR) {
    enum class Level { WARNING, ERROR }
}

annotation class UseExperimental(vararg val markerClass: KClass<out Annotation>)
```

Experimental是个元注解，只能打在注解类上。比如说，我想在代码中引入一项新的实验性功能，名字叫 zzz，就可以像这样：

``` kotlin
@Experimental
annotation class zzz
```

这样zzz就成为了一个「实验性标记」，声明了一个名为 zzz 的范畴。如果某些成员是 zzz 这个实验性功能的内容，就可以使用 zzz 标记它。

``` kotlin
@zzz
fun someFun() {}

@zzz
class SomeClass
```

这样 SomeClass 和 someFun 就是 实验性功能 zzz 的成员了，如果你在其他地方使用了它们，编译器会提醒你正在使用的东西是实验性的。至于怎么提醒，看上面 Experimental 这个注解有一个参数 level，表示提醒的方式，在编译时是发出警告（warring）还是丢出错误（error），默认是编译错误。

``` kotlin
fun test() {
   someFun() // Error: This declaration is experimental and its usage must be marked with '@zzz' or '@UseExperimental(zzz::class)'
}
```

如果要消除这个警告或者错误，就要使用UseExperimental这个注解，表示你已经意识到你正在使用的东西是实验性的。

``` kotlin
@UseExperimental(zzz::class)
fun test() {
   someFun() // ok
}
```

简单的说，在 @UseExperimental(zzz::class) 的作用域内，你可以随意调用 zzz 这个实验性功能的成员。比如说在 kt 文件开头处加上 @file:UseExperimental(zzz::class)，你就可以在整个文件里随便调用 zzz 的成员。

或者可以将 test 也一并纳入 zzz 的范畴，将 test 也声明为实验性功能。

``` kotlin
@zzz
fun test() {
   someFun() // ok
}
```

由此可见，此功能其实非常简单。一个是GPL式传播，另一个则能截断实验性的传播。

另外，Kotlin编译器新添加了两个编译器参数，-Xexperimental和-Xuse-experimental，对整个模块有效。

``` kotlin
// 编译器参数 -Xuse-experimental=zzz
// 等号后接那个注解的完全限定名
fun test() {
   someFun() // もok
}
```

目前 Kotlin Experimental API 也是处在实验性阶段，如果你用到最上那两个标准库里的注解，需要使用编译器参数消除警告。

## 有什么用

在Kotlin 1.3版本目前有3个实验性功能，contract和unsigned integer，所以你想使用这两个功能的话，就会用到上述的 Experimental API。

库作者也可以受益于这项功能（但是如果有改api的话该炸还是会炸的（

本文完。
